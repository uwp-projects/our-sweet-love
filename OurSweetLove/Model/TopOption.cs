﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurSweetLove.Model
{
    public class TopOption
    {
        public string OptionIcon { get; set; }
        public string OptionName { get; set; }
    }

    public class TopOptionManager
    {
        public static List<TopOption> AddOption()
        {
            var options = new List<TopOption>();

            options.Add(new TopOption
            {
                OptionIcon = "\uE70F",
                OptionName = "Edit",

            });
            options.Add(new TopOption
            {
                OptionIcon = "\uE107",
                OptionName = "Delete",

            });

            return options;
        }
    }
}
