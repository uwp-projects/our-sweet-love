﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurSweetLove.Model
{
    public class MenuItem
    {
        public string Icon { get; set; }
        public string ItemName { get; set; }
    }

    public class MenuItemManager
    {
        public static List<MenuItem> AddItems()
        {
            var items = new List<MenuItem>();

            items.Add(new MenuItem
            {
                Icon = "\uEB51",
                ItemName = "Your Love"
            });
            items.Add(new MenuItem
            {
                Icon = "\uE1F6",
                ItemName = "Security"
            });
            items.Add(new MenuItem
            {
                Icon = "\uE72D",
                ItemName = "Share"
            });
            items.Add(new MenuItem
            {
                Icon = "\uE8BD",
                ItemName = "Rate The App"
            });
            items.Add(new MenuItem
            {
                Icon = "\uE2B1",
                ItemName = "Personalize"
            });
            items.Add(new MenuItem
            {
                Icon = "\uE946",
                ItemName = "About"
            });

            return items;
        }
    }
}
